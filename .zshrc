SHRC='$HOME/.zshrc'

bindkey -v

autoload -U promptinit
promptinit
for conffile in `ls ~/.zsh.d/*.conf`; do
	source $conffile
done

export JAVA_HOME=/usr/lib/jvm/java-7-oracle/jre
