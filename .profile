# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# Define the user environment

## History
export HISTFILE=~/.histfile
export HISTSIZE=1000
export SAVEHIST=1000

## Extended PATH
export PATH=$PATH:$HOME/bin

## Default programs
export PAGER=less
export EDITOR=vim
if [ -x /usr/local/bin/st ]; then
    TERMINAL=st
else
    TERMINAL=urxvt
fi
TERMINAL=urxvt
export TERMINAL
export SHELL=/usr/bin/zsh
export NNTPSERVER=news.epita.fr
