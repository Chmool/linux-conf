" vimrc

set nocompatible    " Disable vi compatibility

" Bundles {
        " Setup Bundle Support {
                " The next three lines ensure that the ~/.vim/bundle/ system works
                filetype off
                set rtp+=~/.vim/bundle/Vundle.vim
                call vundle#rc()
        " }

        call vundle#begin() " Start bundle list
        " TODO Add short descrition for each bundle
        Plugin 'gmarik/Vundle.vim'          " The perfect plugin manager
        Plugin 'flazz/vim-colorschemes'     " 3.3M of colorschemes
        Plugin 'scrooloose/nerdtree'        " NERDTree
        Plugin 'jistr/vim-nerdtree-tabs'    " Improve NERDTree
        Plugin 'kien/ctrlp.vim'             " CtrlP
        Plugin 'tpope/vim-surround'         " Surround
        Plugin 'scrooloose/nerdcommenter'   " NERDCommenter
        Plugin 'myusuf3/numbers.vim'        " Numbers
        Plugin 'Shougo/neocomplcache'       " Neocomplcache
        Plugin 'tpope/vim-fugitive'         " Fugituve
        Plugin 'majutsushi/tagbar'          " Tagbar
        Plugin 'Lokaltog/vim-easymotion'    " Easy motion
        Plugin 'bling/vim-airline'          " Airline
        Plugin 'Lokaltog/powerline-fontd'   " Powerline symbols
        Plugin 'scrooloose/syntastic'       " Syntastic
        call vundle#end()  " End bundle list
" }

" General {
        set background=dark " Assume a dark background
        set term=$TERM

        filetype plugin indent on " Automatically detect file types
        syntax on " Syntax highlighting
        set mouse=a " Automatically enable mouse usage
        set mousehide " Hide mouse cursor while typing
        scriptencoding utf-8

        " TODO Handle clipboard
        set clipboard=unnamedplus

        set shortmess=a             " Abbreviation, but no loss of information.
        set history=500             " Longer history
        set spell                   " Spell checking on
        set spelllang=fr,en       " Check french and english syntax
" }

" UI {
        color jellybeans
        hi SpellBad cterm=underline ctermfg=Red ctermbg=Black

        set showmode " FIXME

        set cursorline " Highlight current line

        set ruler
        set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%)
        set showcmd

        set laststatus=2
        let g:airline_powerline_fonts = 1

        set backspace=eol,start,indent
        set whichwrap+=<,>,h,l
        set number
        set showmatch
        set mat=2
        set incsearch
        set nohlsearch
        set ignorecase
        set smartcase
        set wildmenu
        set wildignorecase
        set wildignore=*.o,*~,*.so,*.a
        set scrolloff=9
        set foldenable
        set list
        set listchars=tab:>─,extends:˺,trail:\ ,nbsp:.
" }

" Plugin Setup {
        let g:neocomplcache_enable_at_startup = 1
        "let g:nerdtree_tabs_open_on_console_startup=1

        "let g:tagbar_autofocus = 1
        "let g:tagbar_show_linenumbers = 1
        "let g:tagbar_autoshowtag = 1
        "autocmd FileType * nested :call tagbar#autoopen(0)
        "autocmd BufEnter * nested :call tagbar#autoopen(0)

        let g:syntastic_error_symbol = '✗'
        let g:syntastic_warning_symbol = '⚠'
" }

" Format {
        set tabstop=8
        set softtabstop=4
        set shiftwidth=4

        set textwidth=79

        set wrap
        set smartindent

        set nojoinspaces
        set linebreak

        set expandtab
        set smarttab
" }

" Leader {
        let mapleader = " "
        let g:mapleader = " "

        nmap <leader>w :w!<cr>
        nmap <leader>o o<esc>
        nmap <leader>O O<esc>
        nmap <leader>? :help 

        " Quick window navigation
        nmap <leader>h <C-W>h
        nmap <leader>j <C-W>j
        nmap <leader>k <C-W>k
        nmap <leader>l <C-W>l
        "nmap <leader><leader> <C-W><C-W>

        nmap gs :Gstatus<cr>
" }

" (Re)mapping {
        "nmap :vh :vert<space>help
" }

set foldmarker={,}
set foldmethod=marker
set foldlevelstart=99
set autoread
set splitright
set splitbelow

set lazyredraw

set nobackup
set nowritebackup
set noswapfile

set wrapscan
